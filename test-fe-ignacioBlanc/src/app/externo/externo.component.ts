import { Component, OnInit } from '@angular/core';
import { PeticionesService } from '../services/peticiones.services';

@Component({
  selector: 'app-externo',
  templateUrl: './externo.component.html',
  styleUrls: ['./externo.component.css'],
  providers: [PeticionesService]
})
export class ExternoComponent implements OnInit {

  public user: any;
  public userId: any;
  public users = [];
  


  constructor(
  	private _peticionesService: PeticionesService
  ){
  	this.userId = 1;
  
  }

  ngOnInit() {
    this.cargaUsuario();
   
    this.cargaUsuarios();
    
  }


   
  cargaUsuarios(){
  //	this.users = false;
  	this._peticionesService.getUsers().subscribe(
  		result => {
        this.users = result.data;
        
        this.users.sort(function(a, b){
          var nameA=a.first_name.toLowerCase(), nameB=b.first_name.toLowerCase();
          if (nameA < nameB) //sort string ascending
           return -1;
          if (nameA > nameB)
           return 1;
          return 0; //default return value (no sorting)
         });
           
  		},
  		error => {
  			console.log(<any>error);
  		}
  	);
  }

  cargaUsuario(){
  	this.user = false;
  	this._peticionesService.getUser(this.userId).subscribe(
  		result => {
       
        this.user = result.data;
    
  		},
  		error => {
  			console.log(<any>error);
  		}
  	);
  }

  

}
