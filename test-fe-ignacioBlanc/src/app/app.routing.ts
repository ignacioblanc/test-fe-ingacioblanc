import { ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import { ExternoComponent} from "./externo/externo.component";
import { FormularioComponent} from "./formulario/formulario.component";
import { HomeComponent} from "./home/home.component";

// Array de rutas
const appRoutes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'home', component: HomeComponent},
	{path: 'externo', component: ExternoComponent},
	{path: 'formulario', component: FormularioComponent},
	{path: '**', component: HomeComponent}
];

// Exportar el modulo del router
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);