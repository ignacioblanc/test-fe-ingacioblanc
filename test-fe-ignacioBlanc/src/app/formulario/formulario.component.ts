import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  formulario: FormGroup;
  constructor(
    private fb: FormBuilder
  ) { 
    this.formulario = this.fb.group({
      nombre: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(20)])],
      clave: ['', Validators.required],
      confirmar: ['', Validators.required],
      correo: ['', Validators.required],
      confirmarCorreo: ['', Validators.required]
      
      
    })
  }

  ngOnInit() {
  }

  registro(){

  }

  validacion(){
    const frm = this.formulario.value;
    if(frm.confirmar == frm.clave && frm.confirmarCorreo == frm.correo){ 
      return true;
    }else {
      return false;
    }
   
  }

}
